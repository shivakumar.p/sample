const responseSatus = require('./responseStatus')
const responseMessage = require('./responseMessage')
const requestHandler = {
	"formatRequest":

		function (expressRequest) {
			const formattedRequest = {
				"body": expressRequest.body,
				"query": expressRequest.query,
				"params": expressRequest.params,
				"headers": expressRequest.headers
			};
			return formattedRequest;
		}
		
};

module.exports = requestHandler;