const requestHandler = require(__dirname + "/requestHandler.js");
const responseHandler = require(__dirname + "/responseHandler.js");

exports.parseRequest = requestHandler.formatRequest;
exports.parseResponse = responseHandler.formatResponse;