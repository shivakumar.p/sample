let responseSatus = require('./responseStatus')
let responseMessage = require('./responseMessage')
let responseHandler = {
    "formatResponse":

        function (expressResponse, responseBody) {

            let statusObj = responseSatus[responseBody.responseStatusCode] !== undefined ? responseSatus[responseBody.responseStatusCode] : responseSatus[500];
            let message;
            let data;

            if (responseSatus[responseBody.responseStatusCode] == undefined) {
                message = responseMessage["invalid-satus-code"];
                data = {};
            } else {
                message = responseMessage[responseBody.responseMessageKey] !== undefined ? responseMessage[responseBody.responseMessageKey] : statusObj.type;
                data = responseBody.responseData;
            }
            let responseObject = {
                "status": {
                    "error": statusObj.error,
                    "code": statusObj.code,
                    "type": statusObj.type,
                    "message": message
                },
                "data": data
            };


            expressResponse.status(statusObj.code);
            return responseObject;

        }

};

module.exports = responseHandler;